<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/form', function () {
    return view('form');
});
Route::get('/payments/create', 'PaymentsController@create')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/notifications', 'UserNotificationsController@show')->middleware('auth');
Route::post('/formsubmission', 'FormController@formsub')->middleware('auth');
Route::post('/paymentssubmission', 'PaymentsController@store')->middleware('auth');

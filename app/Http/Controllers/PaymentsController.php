<?php

namespace App\Http\Controllers;

use App\Events\ProductPurchased;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PaymentRecieved;

class PaymentsController extends Controller
{
    public function create() {
        return view('payment');
    }

    public function store() {

        ProductPurchased::dispatch();

        //Notification::send(request()->user(), new PaymentRecieved());
        //The Notification fascade does the same as the below.
        request()->user()->notify(new PaymentRecieved(900));
        return (dd('email sent'));
    }
}

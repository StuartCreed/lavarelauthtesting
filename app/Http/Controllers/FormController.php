<?php

namespace App\Http\Controllers;

use App\Mail\ContactMe;
use App\Mail\TEST;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class FormController extends Controller
{
    public function formsub(){
        $this->authorise('admin');
        Mail::to('stuart.a.creed@gmail.com')
            ->send(new TEST());
        dd('Email sent');
    }
}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'http://laravel6.test/formsubmission',
        'http://laravel6.test/payments/create',
        'http://laravel6.test/paymentssubmission'
    ];
}
